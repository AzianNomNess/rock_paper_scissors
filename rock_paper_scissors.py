#Rock, Paper, Scissors - Evolved
import random, os
from operator import itemgetter
from creatures import Creature

ROCK = 'rock'
PAPER = 'paper'
SCISSORS = 'scissors'

def clear_screen():
    """
    Clear the screen after a match is over.
    """
    os.system('CLS')

def is_valid_input(usr_inpt):
    return usr_inpt and usr_inpt.lower() in (ROCK,PAPER,SCISSORS)

def prompt_user(bad_input=False):
    """
    Welcome message and gather user input.
    We are going to track how many times the user has played already and prompt accordingly.
    """

    if bad_input:
        prompt_bad_input = input("\n\nInvalid input, please select Rock, Paper, or Scissors: ")
        return prompt_bad_input
    else:
        print("\n\nWelcome to Rock, Paper, Scissors!")
        print("-"*33)
        print("Your opponent, a random generator!\n\n")
        user_select = input("Rock, Paper, or Scissors? ")

    return user_select

def battle(usr_crtr, opponent_crtr, user_HP, opponent_HP):
    """
    Handles the attack sequences and turn switching.
    """

    chosen_attack = ""
    opponent_attack = ""
    who_goes_first = random.choice(range(0,2))

    print("You're first to attack!") if who_goes_first == 1 else print("Your opponent attacks first!")

    while user_HP > 0 and opponent_HP > 0:
        if who_goes_first%2 == 1:
            print("\nYour Opponent's HP: {}".format(str(opponent_HP)))
            print("Your Current HP: {}\n\n".format(str(user_HP)))
            print(usr_crtr.creature_attacks())
            chosen_attack = input("Select an attack from your attack list: ").capitalize()
            print()
            try:
                usr_crtr.attack(chosen_attack)
            except KeyError:
                print("That attack is not in your creatures attack list. Please try again.")
                continue
            user_damage_dealt = usr_crtr.attack(chosen_attack)
            opponent_HP -= user_damage_dealt
        elif who_goes_first%2 == 0:
            opponent_attack = random.choice([x for x in iter(opponent_crtr.creature_attack_map)])
            print("{} is attacking!".format(opponent_crtr.creature_name))
            print("{} used {}!".format(opponent_crtr.creature_name,opponent_attack))
            opponent_damage_dealt = opponent_crtr.attack(opponent_attack)
            user_HP -= opponent_damage_dealt
            print("Damage inflicted: {}".format(str(opponent_damage_dealt)))

        who_goes_first += 1
    
    if user_HP <= 0:
        clear_screen()
        print("Your creature {} has been rendered uncontious!".format(usr_crtr.creature_name))
        print("You have lost!")
    elif opponent_HP <= 0:
        clear_screen()
        print("Opponent's creature {} has been rendered uncontious!".format(opponent_crtr.creature_name))
        print("You are victorious!")



def create_creatures():
    """
    Here we will take the user's input, randomly generate an opponent selection, and
    compare the randomly generated opponent selction to a dict of 'user wins' scenarios.
    """

    user_creature_HP = 0
    rand_creature_HP = 0
    user_input = prompt_user()
    rand_options = [ROCK,PAPER,SCISSORS]

    while not is_valid_input(user_input):
        user_input = prompt_user(bad_input=True)
    else:
        if user_input.lower() == ROCK:
            user_creature = Creature(user_input)
            user_creature_HP = user_creature.HP
            user_display_stats = user_creature.display_stats()
            print("\nUSER\n{}\n{}".format('-'*29,user_display_stats))
        elif user_input.lower() == PAPER:
            user_creature = Creature(user_input)
            user_creature_HP = user_creature.HP
            user_display_stats = user_creature.display_stats()
            print("\nUSER\n{}\n{}".format('-'*29,user_display_stats))
        elif user_input.lower() == SCISSORS:
            user_creature = Creature(user_input)
            user_creature_HP = user_creature.HP
            user_display_stats = user_creature.display_stats()
            print("\nUSER\n{}\n{}".format('-'*29,user_display_stats))

    rand_select = random.choice(rand_options)

    if rand_select == ROCK:
        rand_creature = Creature(rand_select)
        rand_creature_HP = rand_creature.HP
        rand_display_stats = rand_creature.display_stats()
        print("\nOPPONENT\n{}\n{}".format('-'*29,rand_display_stats))
    elif rand_select == PAPER:
        rand_creature = Creature(rand_select)
        rand_creature_HP = rand_creature.HP
        rand_display_stats = rand_creature.display_stats()
        print("\nOPPONENT\n{}\n{}".format('-'*29,rand_display_stats))
    elif rand_select == SCISSORS:
        rand_creature = Creature(rand_select)
        rand_creature_HP = rand_creature.HP
        rand_display_stats = rand_creature.display_stats()
        print("\nOPPONENT\n{}\n{}".format('-'*29,rand_display_stats))

    return user_creature,rand_creature,user_creature_HP,rand_creature_HP

if __name__ == '__main__':
    player, opponent, player_health, opponent_health = create_creatures()
    battle(player, opponent, player_health, opponent_health)
else:
    print("This script is not being run correctly.")
