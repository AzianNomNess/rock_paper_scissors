"""
Date: 07/09/2015
Author: Kiyotoshi Ichikawa

We are goning to evolve Rock, Paper, Scissors a bit.
Now, each player (you and the PC) will choose Rock, Paper, or Scissors; each option now represents a creature.
Once a 'creature' has been selected you will do battle.
Going for a pokemon-like game, but of course trying not to rip anything from pokemon.
This is my first program involving classes/objects so it is very basic.


Here specifically we are going to have the blueprints for each creature
"""

from operator import itemgetter

ROCK = 'rock'
PAPER = 'paper'
SCISSORS = 'scissors'


"""
Base class 'Creature', all other classes will inherit from this ABC.
"""
class Creature:
    def __init__(self,selected_creature):
        self.HP = 10000
        self.selected_creature = selected_creature
        self.creature_maps = {ROCK:{'creature_name':'Rockma','creature_type':'rock','armor':8,'base_strength':4,'attack_map':{'Punch':100,'Rock Storm':700,'Crush':300,'Encapsulate':500}},
                    PAPER:{'creature_name':'Papersha','creature_type':'plant','armor':3,'base_strength':2,'attack_map':{'Slap':100,'Paper Slice':700,'Suffocate':300,'Cut':500}},
                    SCISSORS:{'creature_name':'Scizzor','creature_type':'metal','armor':5,'base_strength':3,'attack_map':{'Stab':100,'Fury Blades':700,'Snip':300,'Cut':500}}}
        creature_name = ""

        if self.selected_creature.lower() == ROCK:
            self.creature = self.creature_maps[ROCK]
        elif self.selected_creature.lower() == PAPER:
            self.creature = self.creature_maps[PAPER]
        elif self.selected_creature.lower() == SCISSORS:
            self.creature = self.creature_maps[SCISSORS]

        self.creature_name = self.creature['creature_name']
        self.creature_attack_map = self.creature['attack_map']

    def attack(self, attack_name):
        """
        Returns the amount of damage inflicted based off of the name of the
        attack provided by the user.
        """
        chck_two_wrds = attack_name.split(' ')
        if len(chck_two_wrds) > 1:
            for index,x in enumerate(chck_two_wrds):
                chck_two_wrds[index] = x.capitalize()
        self.attack_name = ' '.join(chck_two_wrds)
        return self.creature['attack_map'][self.attack_name]*self.creature['base_strength']

    def creature_attacks(self):
        """
        Returns the creature's list of attacks and how much damage they inflict.
        """
        attack_names = ""
        attack_damage = ""
        full_attack_list = ""

        """
        The loop here will be used to sort the list of attacks
        by the amount of damage they inflict in decending order.
        
        Formatting of the output is also handled here to make things
        look pretty using if statements to format attack lists specifically
        for each creature.
        """
        if self.creature['creature_name'] == "Rockma":
            for x in sorted(self.creature['attack_map'].items(),key=itemgetter(1)):
                if len(x[0]) == 5:
                    attack_names = "{:>9}/".format(x[0])
                elif len(x[0]) == 11:
                    attack_names = "{:>15}/".format(x[0])
                elif len(x[0]) == 10:
                    attack_names = "{:>14}/".format(x[0])
                attack_damage = "{}\n".format(x[1])
                full_attack_list += (attack_names+attack_damage)
        elif self.creature['creature_name'] == "Papersha":
            for x in sorted(self.creature['attack_map'].items(),key=itemgetter(1)):
                if len(x[0]) == 3:
                    attack_names = "{:>7}/".format(x[0])
                elif len(x[0]) == 4:
                    attack_names = "{:>8}/".format(x[0])
                elif len(x[0]) == 9:
                    attack_names = "{:>13}/".format(x[0])
                elif len(x[0]) == 11:
                    attack_names = "{:>15}/".format(x[0])
                attack_damage = "{}\n".format(x[1])
                full_attack_list += (attack_names+attack_damage)
        elif self.creature['creature_name'] == "Scizzor":
            for x in sorted(self.creature['attack_map'].items(),key=itemgetter(1)):
                if len(x[0]) == 3:
                    attack_names = "{:>7}/".format(x[0])
                elif len(x[0]) == 4:
                    attack_names = "{:>8}/".format(x[0])
                elif len(x[0]) == 11:
                    attack_names = "{:>15}/".format(x[0])
                attack_damage = "{}\n".format(x[1])
                full_attack_list += (attack_names+attack_damage)
        
        return full_attack_list

    def display_stats(self):
        """
        Inherited from Creature class.
        """
        stat_creature_name = "Creature Name: {}".format(self.creature['creature_name'])
        stat_creature_type = "\nCreature Type: {}".format(self.creature['creature_type'])
        stat_creature_starting_HP = "\nCreature Total Health: {}".format(str(self.HP))
        stat_creature_base_strength = "\nCreature Base Strength: {}".format(str(self.creature['base_strength']))
        stat_creature_base_armor = "\nCreature Base Armor: {}".format(str(self.creature['armor']))
        stat_creature_attack_list = "\nCreature Attack List:\n"+self.creature_attacks()
        all_stats = stat_creature_name+stat_creature_type+stat_creature_starting_HP+stat_creature_base_strength+stat_creature_base_armor+stat_creature_attack_list
        return all_stats