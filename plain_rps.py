#Rock, Paper, Scissors
import random, os

def clear_screen():
    '''
    Clear the screen after a match is over.
    '''
    os.system('CLS')

def prompt_user(count=0):
    '''Welcome message and gather user input.
    We are going to track how many times the user has played already and prompt accordingly.
    '''
    if count > 0:
        play_again = input("\n\nWould you like to play again (Yes or No)? ")

        if play_again.lower() == 'yes' or play_again.lower() == 'y':
        	user_select = input("\nRock, Paper, or Scissors? ")
        elif play_again.lower() == 'no' or play_again.lower() == 'n':
            return play_again
        else:
            play_again = 'invalid'
            return play_again

    else:
        print("\n\nWelcome to Rock, Paper, Scissors!")
        print("-"*33)
        print("Your opponent, a random generator!\n\n")
        user_select = input("Rock, Paper, or Scissors? ")

    return user_select


def game_match():
    '''
    Here we will take the user's input, randomly generate an opponent selection, and compare the randomly generated opponent selction to a dict of 'user wins' scenarios.
    '''
    count = 0
    wins = 0
    losses = 0
    ties = 0
    user_input = prompt_user(count)

    while user_input.lower() != 'no' and user_input.lower() != 'n':
        rand_options = ['rock', 'paper', 'scissors']
        user_win_cases = {'rock': 'scissors',
                            'paper': 'rock',
                            'scissors': 'paper'}
        rand_input = random.choice(rand_options)

        try:
        	user_win_cases[user_input.lower()]
        except KeyError:
        	print("\nPlease enter a valid input and try again.")
        	user_input = prompt_user(count)
        	continue

        if rand_input.lower() == user_win_cases[user_input.lower()]:
            print("\n\nUser wins!")
            print("-"*10)
            print()
            print("User Choice: "+user_input.lower())
            print("Random Gen. Choice: "+rand_input.lower())
            print("-"*28)
            print()
            count += 1
            wins += 1
            print("Your Wins: "+str(wins))
            print("Your Losses: "+str(losses))
            print("Your Ties: "+str(ties))
        elif user_input.lower() == rand_input.lower():
            print("\n\nTie!")
            count += 1
            ties += 1
            print("Your Wins: "+str(wins))
            print("Your Losses: "+str(losses))
            print("Your Ties: "+str(ties))
        else:
            print("\n\nUser loses!")
            print("-"*11)
            print()
            print("User Choice: "+user_input.lower())
            print("Random Gen. Choice: "+rand_input.lower())
            count += 1
            losses += 1
            print("Your Wins: "+str(wins))
            print("Your Losses: "+str(losses))
            print("Your Ties: "+str(ties))

        user_input = prompt_user(count)

    clear_screen()

game_match()
